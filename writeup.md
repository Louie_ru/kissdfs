# KISS DFS

## Developers:

- Nikolai Mikriukov - Teamlead. Architecture planning, docker, consultations, code review and refactoring, presentation preparing (commits from Mary is also me)
- Oleg Andriyanchenko - Naming module developer
- Danil Ginzburg - Storage and client modules developer


## Links:

- https://gitlab.com/Louie_ru/kissdfs/
- https://hub.docker.com/r/louieru/kiss_naming
- https://hub.docker.com/r/louieru/kiss_storage
