# KISS DFS

Idea of this project is to make small Distributed File
system on Python3. We followed [Keep It Simple Stupid](https://en.wikipedia.org/wiki/KISS_principle)
principle. The main goal was simplicity rather than design or extra functionality.
Everything should do one simple thing and unnecessary complexity must be avoided.

System allows clients to put, get files from remote system and distributed concept is
hidden from client.


# Installation

KISS DFS supports two modes.

1. Interactive mode. Just run python script. No external libraries is required - pure python3.
Follow instructions on the screen.

```
python3 kiss.py
```

![interactive.png](images/interactive.png)

2. Docker swarm mode. Run docker compose. All images will be downloaded from dockerhub. Your machine should have white ip and clients must use this ip to interact with system. You can also find debug docker compose file nearby. It is used for local testing to run all containers on one machine.

```
docker stack deploy kiss --compose-file docker-compose.yml
```

# Architecture

There are three main modules:

- Naming - coordinator of whole system. One and only instance is necessary for dfs to work.
- Storage - instances where files are stored. Files from clients are distributed among these
nodes. One instance is necessary and two are required for fault tolerance.
- Client - module for users to interact with system

![architecture.png](images/architecture.png)

Naming and storages are located in internal network. Most commands are sent to Naming, but file work (get, put) is done with Storage directly.

![commands.png](images/commands.png)

# Communication

Modules are communicating using raw sockets. Naming module is sending heartbeat periodically
to detect new or broken storages inside internal network. Naming port is always static,
but storage's port might change with every restart. It is sent to Naming during heartbeat
response.

Commands are send using TCP and parameters are splitted by spaces or pipe symbols:

- `cp /dir1/ file1 /dir2/ file2`
- `rm /path/ file1`

Directory path and file name are separated by space. Storages and Naming are using same port
for commands from internal network and clients.


# Help

Client module supports following commands:

- `cd dirpath/`
- `mkdir dirpath/`
- `rmdir dirpath/`
- `ls`
- `put filename`
- `touch filename`
- `get filename`
- `cp path1/filename1 path2/filename2`
- `mv path1/filename1 path2/filename2`
- `rm path1/filename1`
- `info path1/filename1`
- `help`
- `exit`

Commands are similar to usual unix commands. `put` and `get` commands are working with local
file in same directory as on remote system. Local storage is in **client_module/KISS/** folder


# Developers

- Nikolai Mikriukov - Teamlead. Architecture planning, docker, consultations, code review
and refactoring, presentation preparing (commits from Mary is also me)
- Oleg Andriyanchenko - Naming module developer
- Danil Ginzburg - Storage and client modules developer


# Docker images

- https://hub.docker.com/r/louieru/kiss_naming
- https://hub.docker.com/r/louieru/kiss_storage
