import socket
import threading
import common as cn
from os import remove, replace, environ
from shutil import copy, rmtree
import json
from time import sleep


class Client:
    FS_ROOT = 'KISS'
    HELP = "cd dirpath/          - change directory. Use .. to go up\n" \
           "mkdir dirpath/       - make directory\n" \
           "rm dirpath/          - remove directory\n" \
           "ls                   - show files and directories in current folder\n" \
           "put filename         - upload file to system. \n" \
           "                       File should be located in current folder on local machine\n" \
           "touch filename       - create empty file in system\n" \
           "get filename         - download file from remote system.\n" \
           "                       File will be saved to current directory on local machine\n" \
           "cp file_old file_new - copy file\n" \
           "mv file_old file_new - move file\n" \
           "rm filename          - remove file\n" \
           "info filename        - show file info\n" \
           "help                 - show this help message\n" \
           "exit                 - close client module\n"

    def __init__(self, naming_ip):
        # initialize folder for files
        cn.create_dirs(Client.FS_ROOT + '/')
        self.naming_ip = naming_ip
        self.current_dir = '/'
        threading.Thread(target=self.console).start()

    def check_dir_exists(self, dirpath: str) -> bool:
        response = cn.request(b'cd ' + dirpath.encode(), (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def make_dir(self, dirpath: str) -> bool:
        response = cn.request(b'mkdir ' + dirpath.encode(), (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def rm_dir(self, dirpath: str) -> bool:
        response = cn.request(b'rmdir ' + dirpath.encode(), (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def list_files(self) -> list or None:
        response = cn.request(b'ls ' + self.current_dir.encode(), (self.naming_ip, cn.NAMING_PORT)).decode()
        try:
            return json.loads(response)
        except json.decoder.JSONDecodeError:
            return None

    def get_file(self, filepath) -> bool:
        # request Naming to find Storage with that file
        dirpath, filename = self.split_filepath(filepath)
        storage = cn.request(f"where {dirpath} {filename}".encode(), (self.naming_ip, cn.NAMING_PORT)).decode()
        storage_ip, storage_port = storage.split()
        # download file from Storage
        msg = f"get {dirpath} {filename}".encode()
        file_contents = cn.request(msg, (storage_ip, int(storage_port)))
        cn.create_dirs(self.FS_ROOT + dirpath)
        # write file to disk
        with open(self.FS_ROOT + filepath, "wb") as file:
            file.write(file_contents)
        return True

    def put_file(self, filepath, file_contents) -> bool:
        # take any random storage
        dirpath, filename = self.split_filepath(filepath)
        storage = cn.request(b"random_storage", (self.naming_ip, cn.NAMING_PORT)).decode()
        storage_ip, storage_port = storage.split()
        if storage_ip == b'EMPTY':
            return False  # no storages
        # put file to remote storage
        storage_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        storage_sock.connect((storage_ip, int(storage_port)))
        msg = f"put {dirpath} {filename}".encode()
        cn.send_conn(storage_sock, msg)
        sleep(0.5)  # send file contents in next segment
        cn.send_conn(storage_sock, file_contents)
        storage_sock.close()
        return True

    def rm_file(self, filepath: str) -> bool:
        dirpath, filename = self.split_filepath(filepath)
        response = cn.request(f'rm {dirpath} {filename}'.encode(), (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def cp_file(self, filepath: str, filepath_new: str) -> bool:
        dirpath, filename = self.split_filepath(filepath)
        dirpath_new, filename_new = self.split_filepath(filepath_new)
        response = cn.request(f'cp {dirpath} {filename} {dirpath_new} {filename_new}'.encode(),
                              (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def mv_file(self, filepath: str, filepath_new: str) -> bool:
        dirpath, filename = self.split_filepath(filepath)
        dirpath_new, filename_new = self.split_filepath(filepath_new)
        response = cn.request(f'mv {dirpath} {filename} {dirpath_new} {filename_new}'.encode(),
                              (self.naming_ip, cn.NAMING_PORT))
        return response == b'OK'

    def info_file(self, filepath: str) -> list or None:
        # find storage with that file
        dirpath, filename = self.split_filepath(filepath)
        storage = cn.request(f"where {dirpath} {filename}".encode(), (self.naming_ip, cn.NAMING_PORT)).decode()
        # ask storage about that file
        storage_ip, storage_port = storage.split()
        response = cn.request(f'info {dirpath} {filename}'.encode(), (storage_ip, int(storage_port))).decode()
        try:
            return json.loads(response)
        except json.decoder.JSONDecodeError:
            return None

    @staticmethod
    def calculate_dirpath(old_dir, new_dir):
        # given current directory and cd command parameter, calculate new directory path
        if new_dir[0] == '/':
            # absolute path
            new_path = new_dir
        elif new_dir == '..':
            # up one directory
            if old_dir == '/':
                new_path = old_dir
            else:
                last_dir_index = old_dir[:-1].rfind('/')
                new_path = old_dir[:last_dir_index + 1]
        else:
            # relative path
            new_path = old_dir + new_dir
        # make sure last symbol is '/'
        if new_path[-1] != '/':
            new_path += '/'
        return new_path

    @staticmethod
    def calculate_filepath(old_dir, filepath):
        # given old directory and file path or name, calculate full file path
        if filepath[0] == '/':
            # absolute path
            new_path = filepath
        else:
            # relative path
            new_path = old_dir + filepath
        return new_path

    @staticmethod
    def split_filepath(filepath):
        # split path to file on directory and filename
        last_dir_index = filepath.rfind('/')
        dirpath = filepath[:last_dir_index + 1]
        filename = filepath[last_dir_index + 1:]
        return dirpath, filename

    def console(self) -> None:
        # process client's input
        print('Console is started')
        while True:
            params = input(f'{self.current_dir}$ ').split()
            if params[0] == 'cd':
                # command format: "cd new_dir"
                new_path = self.calculate_dirpath(self.current_dir, params[1])
                if self.check_dir_exists(new_path):
                    self.current_dir = new_path
                else:
                    print('ERROR')
            elif params[0] == 'mkdir':
                # command format: "mkdir new_dir"
                new_path = self.calculate_dirpath(self.current_dir, params[1])
                if self.make_dir(new_path):
                    cn.create_dirs(Client.FS_ROOT + new_path)
                else:
                    print('ERROR')
            elif params[0] == 'rmdir':
                # command format: "rmdir dir"
                new_path = self.calculate_dirpath(self.current_dir, params[1])
                if self.rm_dir(new_path):
                    try:
                        rmtree(Client.FS_ROOT + new_path)
                    except FileNotFoundError:
                        pass
                else:
                    print('ERROR')
            elif params[0] == 'ls':
                # command format: "ls"
                response = self.list_files()
                if response is None:
                    print("Error")
                    continue
                try:
                    files, dirs = response
                except TypeError:
                    continue
                if len(files) > 0:
                    files = sorted(list({f[2] for f in files}))
                    print(' '.join(files))
                if len(dirs) > 0:
                    print(' '.join(sorted(dirs)))
            elif params[0] == 'get':
                # command format: "get filepath"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                if self.get_file(filepath):
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'rm':
                # command format: "rm filepath"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                if self.rm_file(filepath):
                    try:
                        remove(self.FS_ROOT + filepath)
                    except FileNotFoundError:
                        pass
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'mv':
                # command format: "mv filepath filepath_new"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                filepath_new = self.calculate_filepath(self.current_dir, params[2])
                if self.mv_file(filepath, filepath_new):
                    try:
                        replace(self.FS_ROOT + filepath, self.FS_ROOT + filepath_new)
                    except FileNotFoundError:
                        pass
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'cp':
                # command format: "cp filepath filepath_new"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                filepath_new = self.calculate_filepath(self.current_dir, params[2])
                if self.cp_file(filepath, filepath_new):
                    try:
                        copy(self.FS_ROOT + filepath, self.FS_ROOT + filepath_new)
                    except FileNotFoundError:
                        pass
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'info':
                # command format: "info filepath"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                info = self.info_file(filepath)
                if info:
                    print(f'{info[1]}{info[2]} {info[0]} {info[3]}')
                else:
                    print('ERROR')
            elif params[0] == 'put':
                # command format: "put filepath"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                with open(self.FS_ROOT + filepath, "rb") as file:
                    contents = file.read()
                if self.put_file(filepath, contents):
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'touch':
                # command format: "touch filepath"
                filepath = self.calculate_filepath(self.current_dir, params[1])
                if self.put_file(filepath, b''):
                    with open(self.FS_ROOT + filepath, "wb"):
                        pass
                    print("OK")
                else:
                    print('ERROR')
            elif params[0] == 'help':
                print(Client.HELP)
            elif params[0] == 'exit':
                exit(0)
            else:
                print("ERROR: Unknown command")


if __name__ == '__main__':
    naming_ip = input("Enter naming ip: \n")
    client = Client(naming_ip)
