import socket
import threading
from time import sleep
import json
from random import choice
from json import dumps
from os import popen

import naming.naming_db as db
import common as cn


class Naming:
    HEARTBEAT_CYCLE = 3  # scan every N seconds
    HEARTBEAT_AWAIT = 1  # all alive storages should response in that time
    REPLICATE_CYCLE = 5  # check for replication every N seconds

    def __init__(self, internal_ip, broadcast):
        print("Initializing database")
        db.init_database()
        self.alive_storages = {}
        self.new_storages = {}
        print(f"Setting network config with ip {internal_ip} and broadcast {broadcast}")
        self.internal_ip = internal_ip
        self.broadcast_address = (broadcast, cn.HEARTBEAT_PORT)
        threading.Thread(target=cn.connection_receiver, args=(cn.NAMING_PORT, self.command_processor)).start()
        threading.Thread(target=self.heartbeat).start()
        threading.Thread(target=self.replicate_check).start()

    def heartbeat(self) -> None:
        # scan network for storage_module servers
        sleep(0.2)
        upd_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        upd_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        upd_server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        print('Heartbeat sender is started')

        while True:
            self.new_storages = dict()
            msg = f'heartbeat {self.internal_ip}'.encode()
            # send scan to everyone
            upd_server.sendto(msg, self.broadcast_address)

            sleep(self.HEARTBEAT_AWAIT)
            prev_alive_storages = set(self.alive_storages.keys())
            new_alive_storages = set(self.new_storages.keys())

            storages_died = prev_alive_storages - new_alive_storages
            storages_new = new_alive_storages - prev_alive_storages
            storages_updated = prev_alive_storages.intersection(new_alive_storages)

            # update storages in db
            for port in storages_died:
                print(f"Storage {port} died")
                db.remove_storage(port)
            for port in storages_new:
                print(f"Storage {port} found")
                db.update_storage_files(port, self.new_storages[port])
                # update files for storage
                for file in self.new_storages[port]:
                    if db.is_dead(file[0], file[1]):
                        # remove file if was removed during storage downtime
                        data = f'rm {file[0]} {file[1]}'.encode()
                        global_ip, local_ip = db.get_storage(port)
                        cn.send_addr(local_ip, port, data)
            for port in storages_updated:
                db.update_storage_files(port, self.new_storages[port])
            self.alive_storages = self.new_storages.copy()
            sleep(self.HEARTBEAT_CYCLE)

    def replicate_check(self):
        # check each file is at least on 2 storages. Replicate if needed
        while True:
            sleep(self.REPLICATE_CYCLE)
            all_files = db.all_files()
            files_struct = {}  # (dir, filename) = [(storage1, updated1), (storage2, updated2)]
            # calculate needed structure for future processing
            for file in all_files:
                port, dirpath, filename, updated = file
                if (dirpath, filename) in files_struct:
                    files_struct[(dirpath, filename)].append((port, updated))
                else:
                    files_struct[(dirpath, filename)] = [(port, updated)]

            for filepath in files_struct:
                dirpath, filename = filepath
                if len(files_struct[filepath]) == 1 and len(self.alive_storages.keys()) > 1:
                    # only one copy is found
                    # find another storage to replicate
                    storage_port, updated = files_struct[filepath][0]
                    global_ip, local_ip = db.get_storage(storage_port)
                    repl_port = (set(self.alive_storages.keys()) - {storage_port}).pop()
                    repl_global_ip, repl_local_ip = db.get_storage(repl_port)
                    cn.send_addr(repl_local_ip, repl_port,
                                 f"replicate {local_ip}|{storage_port}|{dirpath}|{filename}|{updated}".encode())
                if len(files_struct[filepath]) == 2:
                    # some replica file is outdated
                    # say to replicate
                    timestamp1, timestamp2 = files_struct[filepath][0][1], files_struct[filepath][1][1]
                    if timestamp1 != timestamp2:
                        storage_port1, updated1 = files_struct[filepath][0]
                        storage_port2, updated2 = files_struct[filepath][1]
                        global_ip1, local_ip1 = db.get_storage(storage_port1)
                        global_ip2, local_ip2 = db.get_storage(storage_port2)
                        if timestamp1 > timestamp2:
                            cn.send_addr(local_ip2, storage_port2,
                                         f"replicate {local_ip1}|{storage_port1}|"
                                         f"{dirpath}|{filename}|{timestamp1}".encode())
                        else:
                            cn.send_addr(local_ip1, storage_port1,
                                         f"replicate {local_ip2}|{storage_port2}|"
                                         f"{dirpath}|{filename}|{timestamp2}".encode())

    def command_processor(self, conn: socket.socket, addr: tuple) -> None:
        data = cn.receive_data(conn)
        if not data.startswith(b'heartbeat'):
            print("cmd:", data.decode())

        # --- commands from storage ---

        if data.startswith(b'heartbeat'):
            # storage replied
            # command format: "heartbeat external_storage_ip [[dir1,file1,timestamp1],[dir2,file2,timestamp2],...]"
            init_data = data[10:].decode()
            storage_global_ip, storage_port, storage_files = init_data.split('|')
            storage_local_ip = addr[0]
            storage_port = int(storage_port)
            db.update_storage_ips(storage_port, storage_global_ip, storage_local_ip)
            # update storage_module node in list (will pe pushed to database inside heartbeat scan)
            self.new_storages[storage_port] = json.loads(storage_files)
        elif data.startswith(b'put'):
            # command format: "put dirpath/ filename storage_ip"
            # storage_module notify about file put
            params = data[4:].decode()
            dirpath, filename, storage_port = params.split()
            db.make_alive(dirpath, filename)
            db.update_file(storage_port, dirpath, filename)

        # --- commands from client ---

        elif data.startswith(b'cd'):
            # command format: "cd dirpath/"
            # client always sends absolute path. Check such directory exists
            dirpath = data[3:].decode()
            if db.dir_exists(dirpath):
                cn.send_conn(conn, b'OK')
            else:
                cn.send_conn(conn, b'ERROR: directory does not exist')
        elif data.startswith(b'mkdir'):
            # command format: "mkdir dirpath/"
            dirpath = data[6:].decode()
            if db.dir_exists(dirpath):
                cn.send_conn(conn, b'ERROR: directory already exists')
            else:
                db.make_dir(dirpath)
                cn.send_conn(conn, b'OK')
        elif data.startswith(b'rmdir'):
            # command format: "rmdir dirpath/"
            dirpath = data[6:].decode()
            if not db.dir_exists(dirpath):
                cn.send_conn(conn, b'ERROR: directory does not exist')
            else:
                db.rm_dir(dirpath)
                for storage_port in self.alive_storages:
                    global_ip, local_ip = db.get_storage(storage_port)
                    cn.send_addr(local_ip, storage_port, f'rmdir {dirpath}'.encode())
                cn.send_conn(conn, b'OK')
        elif data.startswith(b'ls'):
            # command format: "ls dirpath/"
            # send files in directory
            dirpath = data[3:].decode()
            if not db.dir_exists(dirpath):
                cn.send_conn(conn, b'ERROR: directory does not exist')
            else:
                files = db.list_files_in_dir(dirpath)
                dirs = db.list_dirs_in_dir(dirpath)
                cn.send_conn(conn, dumps([files, dirs]).encode())
        elif data == b'random_storage':
            # send random storage_module ip for new file push
            ports = list(self.alive_storages.keys())
            if not ports:
                cn.send_conn(conn, b'ERROR')
            else:
                port = choice(ports)
                global_ip, _ = db.get_storage(port)
                cn.send_conn(conn, f"{global_ip} {port}".encode())
        elif data.startswith(b'where'):
            # command format: "where dirpath/ filename"
            # send storage_module ip where file is stored
            filepath = data[6:].decode()
            dirpath, filename = filepath.split()
            port = db.storages_by_file(dirpath, filename)[0]
            global_ip, _ = db.get_storage(port)
            cn.send_conn(conn, f"{global_ip} {port}".encode())
        elif data.startswith(b'rm '):
            # command format: "rm dirpath/ filename"
            # remove file
            filepath = data[3:].decode()
            dirpath, filename = filepath.split()
            # remove file on all storages
            for storage_port in db.storages_by_file(dirpath, filename):
                data = f'rm {dirpath} {filename}'.encode()
                global_ip, local_ip = db.get_storage(storage_port)
                cn.send_addr(local_ip, storage_port, data)
            db.rm_file(dirpath, filename)
            cn.send_conn(conn, b'OK')
        elif data.startswith(b'mv '):
            # command format: "mv dirpath/ filename dirpath_new/ filename_new"
            # move file
            filepaths = data[3:].decode()
            dirpath, filename, dirpath_new, filename_new = filepaths.split()
            # move file on all storages
            for storage_port in db.storages_by_file(dirpath, filename):
                data = f'mv {dirpath} {filename} {dirpath_new} {filename_new}'.encode()
                global_ip, local_ip = db.get_storage(storage_port)
                cn.send_addr(local_ip, storage_port, data)
            db.mv_file(dirpath, filename, dirpath_new, filename_new)
            cn.send_conn(conn, b'OK')
        elif data.startswith(b'cp '):
            # command format: "cp dirpath/ filename dirpath_new/ filename_new"
            # copy file
            filepaths = data[3:].decode()
            dirpath, filename, dirpath_new, filename_new = filepaths.split()
            # copy file on all storages
            for storage_port in db.storages_by_file(dirpath, filename):
                data = f'cp {dirpath} {filename} {dirpath_new} {filename_new}'.encode()
                global_ip, local_ip = db.get_storage(storage_port)
                cn.send_addr(local_ip, storage_port, data)
            db.cp_file(dirpath, filename, dirpath_new, filename_new)
            cn.send_conn(conn, b'OK')
        conn.close()


if __name__ == '__main__':
    # use `ip a` command to determine addresses
    internal_ip = popen("ip a|awk '/42.42.42/ {print $2}'").read().split('/')[0]
    broadcast = popen("ip a|awk '/42.42.42/ {print $4}'").read()[:-1]
    naming = Naming(internal_ip, broadcast)
