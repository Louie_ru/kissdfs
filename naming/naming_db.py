import sqlite3
import threading

DB_NAME = 'naming/naming.sqlite'
lock = threading.RLock()  # at any moment of time only one thread may write to db. All others are waiting
conn = sqlite3.connect(DB_NAME, check_same_thread=False)
cursor = conn.cursor()  # cursor allows to iterate over database data


def init_database():
    with lock:
        cursor.execute("""CREATE TABLE IF NOT EXISTS dirs (
                            dirpath TEXT PRIMARY KEY
                          )""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS death_certs (
                            dirpath TEXT NOT NULL,
                            filename TEXT NOT NULL,
                            PRIMARY KEY (dirpath, filename)
                          )""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS files (
                            storage_port INTEGER,
                            dirpath TEXT NOT NULL,
                            filename TEXT NOT NULL,
                            updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY (storage_port, dirpath, filename)
                          )""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS storages (
                            storage_port INTEGER NOT NULL,
                            global_ip TEXT NOT NULL,
                            local_ip TEXT NOT NULL,
                            PRIMARY KEY (storage_port)
                          )""")
        cursor.execute("INSERT OR IGNORE INTO dirs VALUES ('/')")
        cursor.execute("DELETE FROM files")
        conn.commit()


def remove_storage(storage_port):
    with lock:
        cursor.execute("DELETE FROM files WHERE storage_port =?", (storage_port,))
        conn.commit()


def update_storage_files(storage_port, files):
    remove_storage(storage_port)
    with lock:
        for file in files:
            if not is_dead(file[0], file[1]):
                cursor.execute("INSERT INTO files VALUES (?, ?, ?, ?)", (storage_port, file[0], file[1], file[2]))
                cursor.fetchall()
        conn.commit()


def update_storage_ips(port, global_ip, local_ip):
    with lock:
        if get_storage(port):
            cursor.execute("UPDATE storages SET global_ip = ?, local_ip = ? WHERE storage_port = ?",
                           (global_ip, local_ip, port))
        else:
            cursor.execute("INSERT INTO storages VALUES (?, ?, ?)", (port, global_ip, local_ip))
        conn.commit()


def dir_exists(dirpath):
    with lock:
        cursor.execute("SELECT COUNT(*) FROM dirs WHERE dirpath LIKE ?", (f'{dirpath}%',))
        tmp = cursor.fetchone()
    return tmp[0] > 0


def make_dir(dirpath):
    with lock:
        cursor.execute("INSERT INTO dirs VALUES (?)", (dirpath,))
        conn.commit()


def rm_dir(dirpath):
    with lock:
        cursor.execute("DELETE FROM dirs WHERE dirpath LIKE ?", (f'{dirpath}%',))
        conn.commit()


def list_files_in_dir(dirpath):
    with lock:
        cursor.execute("SELECT * FROM files WHERE dirpath = ? AND NOT EXISTS "
                       "(SELECT * FROM death_certs WHERE death_certs.dirpath = files.dirpath "
                       "AND death_certs.filename = files.filename)", (dirpath,))
        return cursor.fetchall()


def all_files():
    with lock:
        cursor.execute("SELECT * FROM files")
        return cursor.fetchall()


def list_dirs_in_dir(dirpath):
    with lock:
        cursor.execute("SELECT * FROM dirs WHERE dirpath LIKE ?", (f"{dirpath}_%",))
        dirs = cursor.fetchall()
    # cut full path to name in dir (ex. /dirpath/test/ to test/)
    dirnames = [d[0][len(dirpath):].split('/')[0] + '/' for d in dirs]
    return list(set(dirnames))


def file_info(dirpath, filename):
    with lock:
        cursor.execute("SELECT * FROM files WHERE dirpath = ? AND filename = ?", (dirpath, filename))
        return cursor.fetchone()


def rm_file(dirpath, filename):
    with lock:
        cursor.execute("DELETE FROM files WHERE dirpath = ? AND filename = ?", (dirpath, filename))
        cursor.execute("INSERT OR IGNORE INTO death_certs VALUES (?, ?)", (dirpath, filename))
        conn.commit()


def mv_file(dirpath, filename, dirpath_new, filename_new):
    make_alive(dirpath_new, filename_new)
    with lock:
        cursor.execute("UPDATE files SET dirpath = ?, filename = ?, updated = CURRENT_TIMESTAMP "
                       "WHERE dirpath = ? AND filename = ?", (dirpath_new, filename_new, dirpath, filename))
        conn.commit()


def file_exists(dirpath, filename):
    with lock:
        cursor.execute("SELECT COUNT(*) FROM files WHERE dirpath = ? AND filename = ? LIMIT 1", (dirpath, filename))
        return cursor.fetchone()[0] == 1


def update_file(storage_port, dirpath, filename):
    with lock:
        if file_exists(dirpath, filename):
            cursor.execute("UPDATE files SET updated = CURRENT_TIMESTAMP WHERE dirpath = ? AND filename = ? "
                           "AND storage_port = ?", (dirpath, filename, storage_port))
        else:
            cursor.execute("INSERT INTO files VALUES (?, ?, ?, CURRENT_TIMESTAMP)", (storage_port, dirpath, filename))
        conn.commit()


def cp_file(dirpath, filename, dirpath_new, filename_new):
    storages = storages_by_file(dirpath, filename)
    make_alive(dirpath_new, filename_new)
    with lock:
        for storage in storages:
            cursor.execute("INSERT INTO files VALUES (?, ?, ?, CURRENT_TIMESTAMP)",
                           (storage, dirpath_new, filename_new))

        conn.commit()


def random_storage():
    with lock:
        cursor.execute("SELECT DISTINCT storage_port FROM files ORDER BY RANDOM() LIMIT 1")
        storage_row = cursor.fetchone()
    return storage_row[0] if storage_row else None


def storages_by_file(dirpath, filename):
    with lock:
        cursor.execute("SELECT storage_port FROM files WHERE dirpath = ? and filename = ?", (dirpath, filename))
        storage_rows = cursor.fetchall()
    return [s[0] for s in storage_rows] if storage_rows else None


def get_storage(port):
    with lock:
        cursor.execute("SELECT global_ip, local_ip FROM storages WHERE storage_port = ?", (port,))
        return cursor.fetchone()


def is_dead(dirpath, filename):
    with lock:
        cursor.execute("SELECT COUNT(*) FROM death_certs WHERE dirpath = ? and filename = ?", (dirpath, filename))
        result = cursor.fetchone()
    return result[0] > 0


def make_alive(dirpath, filename):
    with lock:
        cursor.execute("DELETE FROM death_certs WHERE dirpath = ? and filename = ?", (dirpath, filename))
        conn.commit()
