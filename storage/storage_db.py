import sqlite3
from threading import RLock


DB_NAME = 'storage/storage.sqlite'
conn = sqlite3.connect(DB_NAME, check_same_thread=False)
cursor = conn.cursor()  # cursor allows to iterate over database data
lock = RLock()  # at any moment of time only one thread may write to db. All others are waiting


def init_database():
    with lock:
        cursor.execute("""CREATE TABLE IF NOT EXISTS files (
                            dirpath TEXT NOT NULL,
                            filename TEXT NOT NULL,
                            updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                            PRIMARY KEY (dirpath, filename)
                          )""")
        conn.commit()


def all_files():
    with lock:
        cursor.execute("SELECT * FROM files")
        return cursor.fetchall()


def exists_file(dirpath, filename) -> bool:
    with lock:
        cursor.execute("SELECT COUNT(*) FROM files WHERE dirpath = ? AND filename = ? LIMIT 1", (dirpath, filename))
        return cursor.fetchone()[0] == 1


def info_file(dirpath, filename):
    with lock:
        cursor.execute("SELECT * FROM files WHERE dirpath = ? AND filename = ?", (dirpath, filename))
        return cursor.fetchone()


def update_file(dirpath, filename, timestamp=None):
    with lock:
        if exists_file(dirpath, filename):
            if timestamp is None:
                cursor.execute("UPDATE files SET updated = CURRENT_TIMESTAMP WHERE dirpath = ? AND filename = ? ",
                               (dirpath, filename))
            else:
                cursor.execute("UPDATE files SET updated = ? WHERE dirpath = ? AND filename = ? ",
                               (timestamp, dirpath, filename))
        else:
            if timestamp is None:
                cursor.execute("INSERT INTO files VALUES (?, ?, CURRENT_TIMESTAMP)", (dirpath, filename))
            else:
                cursor.execute("INSERT INTO files VALUES (?, ?, ?)", (dirpath, filename, timestamp))
        conn.commit()


def rm_file(dirpath, filename):
    with lock:
        cursor.execute("DELETE FROM files WHERE dirpath = ? AND filename = ?", (dirpath, filename))
        conn.commit()


def rm_dir(dirpath):
    with lock:
        cursor.execute("DELETE FROM files WHERE dirpath = ?", (dirpath,))
        conn.commit()


def mv_file(dirpath, filename, dirpath_new, filename_new):
    with lock:
        cursor.execute("UPDATE files SET dirpath = ?, filename = ?, updated = CURRENT_TIMESTAMP "
                       "WHERE dirpath = ? AND filename = ?", (dirpath_new, filename_new, dirpath, filename))
        conn.commit()
