import socket
import threading
from random import randint
from os import remove, replace, path, environ
from shutil import copy, rmtree
from time import sleep
from json import dumps
from urllib.request import urlopen

import storage.storage_db as db
import common as cn


class Storage:
    FS_ROOT = 'storage/KISS'  # where files are stored

    def __init__(self, is_global):
        db.init_database()
        cn.create_dirs(Storage.FS_ROOT + '/')
        if is_global:
            self.external_ip = urlopen('https://ident.me').read().decode()
        else:
            self.external_ip = '127.0.0.1'
        self.port = randint(50001, 51000)
        print(f"Starting on port {self.port} and ip {self.external_ip}")
        self.naming_ip = None  # initialized during heartbeat
        threading.Thread(target=cn.connection_receiver, args=(self.port, self.command_processor)).start()
        threading.Thread(target=self.heartbeat_listener).start()

    def heartbeat_listener(self):
        sleep(0.2)
        udp_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        udp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        udp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        udp_server.bind(('', cn.HEARTBEAT_PORT))
        print("Heartbeat listener is started")

        while True:
            data, addr = udp_server.recvfrom(1024)
            # heartbeat format: "heartbeat naming_internal_ip"
            if not data.startswith(b'heartbeat'):
                continue
            if self.naming_ip is None:
                print("Received broadcast from Naming server")
            self.naming_ip = data.decode().split()[1]
            files = db.all_files()
            msg = f'heartbeat {self.external_ip}|{self.port}|{dumps(files)}'.encode()
            cn.send_addr(self.naming_ip, cn.NAMING_PORT, msg)

    def command_processor(self, conn: socket.socket, _) -> None:
        data = cn.receive_data(conn)
        print("cmd:", data.decode())
        # --- client commands ---

        if data.startswith(b'put '):
            # command format: "put dirpath/ filename", "file_content"
            filepath = data[4:].decode()
            dirpath, filename = filepath.split()
            file_contents = cn.receive_data(conn)
            cn.create_dirs(Storage.FS_ROOT + dirpath)
            with open(Storage.FS_ROOT + dirpath + filename, "wb") as file:
                file.write(file_contents)
            db.update_file(dirpath, filename)
            msg = f'put {dirpath} {filename} {self.port}'.encode()
            cn.send_addr(self.naming_ip, cn.NAMING_PORT, msg)
        elif data.startswith(b'get '):
            # command format: "get dirpath/ filename"
            filepath = data[4:].decode()
            dirpath, filename = filepath.split()
            if not db.exists_file(dirpath, filename):
                cn.send_conn(conn, b"ERROR: file does not exist")
            else:
                with open(Storage.FS_ROOT + dirpath + filename, "rb") as file:
                    cn.send_conn(conn, file.read())
        elif data.startswith(b'info '):
            # command format: "info dirpath/ filename"
            # info about file
            filepath = data[5:].decode()
            dirpath, filename = filepath.split()
            if db.exists_file(dirpath, filename):
                info = list(db.info_file(dirpath, filename))
                file_size = path.getsize(Storage.FS_ROOT + dirpath + filename)
                info = [file_size] + info
                cn.send_conn(conn, dumps(info).encode())

        # --- naming commands ---
        elif data.startswith(b'rm '):
            # command format: "rm dirpath/ filename"
            # remove file
            filepath = data[3:].decode()
            dirpath, filename = filepath.split()
            if db.exists_file(dirpath, filename):
                try:
                    remove(Storage.FS_ROOT + dirpath + filename)
                except FileNotFoundError:
                    pass
                db.rm_file(dirpath, filename)
        elif data.startswith(b'rmdir'):
            # command format: "rm dirpath/ filename"
            # remove dir
            dirpath = data[6:].decode()
            db.rm_dir(dirpath)
            try:
                rmtree(Storage.FS_ROOT + dirpath)
            except FileNotFoundError:
                pass
        elif data.startswith(b'mv '):
            # command format: "mv dirpath/ filename dirpath_new/ filename_new"
            # move file
            filepaths = data[3:].decode()
            dirpath, filename, dirpath_new, filename_new = filepaths.split()
            if db.exists_file(dirpath, filename):
                cn.create_dirs(Storage.FS_ROOT + dirpath_new)
                try:
                    replace(Storage.FS_ROOT + dirpath + filename, Storage.FS_ROOT + dirpath_new + filename_new)
                except FileNotFoundError:
                    pass
                db.mv_file(dirpath, filename, dirpath_new, filename_new)
        elif data.startswith(b'cp '):
            # command format: "cp dirpath/ filename dirpath_new/ filename_new"
            # copy file
            filepaths = data[3:].decode()
            dirpath, filename, dirpath_new, filename_new = filepaths.split()
            if db.exists_file(dirpath, filename):
                cn.create_dirs(Storage.FS_ROOT + dirpath_new)
                try:
                    copy(Storage.FS_ROOT + dirpath + filename, Storage.FS_ROOT + dirpath_new + filename_new)
                except FileNotFoundError:
                    pass
                db.update_file(dirpath_new, filename_new)
        elif data.startswith(b'replicate'):
            # command format: "replicate storage_ip storage_port dirpath/ filename timestamp"
            # replicate file from another storage
            filepaths = data[10:].decode()
            storage_ip, storage_port, dirpath, filename, timestamp = filepaths.split('|')

            msg = f"get {dirpath} {filename}".encode()
            file_contents = cn.request(msg, (storage_ip, int(storage_port)))
            cn.create_dirs(self.FS_ROOT + dirpath)
            with open(self.FS_ROOT + dirpath + filename, "wb") as file:
                file.write(file_contents)
            db.update_file(dirpath, filename, timestamp)
        conn.close()


if __name__ == '__main__':
    storage = Storage()
