import socket
from os import makedirs
import threading

NAMING_PORT = 50000
HEARTBEAT_PORT = 50001
END_TRAILER = b'END_TRAILER'


def receive_data(sock: socket.socket) -> bytes:
    # receive until trailer comes
    data = b''
    while not data.endswith(END_TRAILER):
        data += sock.recv(1024)
    return data[:-len(END_TRAILER)]


def send_addr(ip: str, port: int, data: bytes):
    # send data to given address
    storage_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        storage_sock.connect((ip, port))
    except ConnectionRefusedError:
        return
    storage_sock.sendall(data + END_TRAILER)  # append trailer
    storage_sock.close()


def send_conn(conn: socket.socket, data: bytes):
    # send data to socket
    conn.sendall(data + END_TRAILER)


def create_dirs(filepath):
    # create all dirs to path
    last_dir_index = filepath.rfind('/')
    new_path = filepath[:last_dir_index + 1]
    try:
        makedirs(new_path)
    except FileExistsError:
        pass


def connection_receiver(port: int, func) -> None:
    # wait for connections and process each request in separate thread
    tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tcp_server.bind(('', port))
    tcp_server.listen(5)
    print('Command listener is started')

    while True:
        conn, addr = tcp_server.accept()
        # Start thread to process command
        threading.Thread(target=func, args=(conn, addr), daemon=True).start()


def request(data: bytes, addr: tuple) -> bytes:
    # request command and return response
    naming_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        naming_sock.connect(addr)
    except ConnectionRefusedError:
        return b'ERROR: Connection refused'
    naming_sock.sendall(data + END_TRAILER)
    response = receive_data(naming_sock)
    naming_sock.close()
    return response
