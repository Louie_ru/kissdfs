from os import popen
from re import findall
import argparse

from client.client import Client
from storage.storage import Storage
from naming.naming import Naming

parser = argparse.ArgumentParser()
parser.add_argument("--module", help="0 - Naming; 1 - Storage; 2 - Client", type=int)
parser.add_argument("--internal_ip", help="Naming: local ip for internal network", type=str)
parser.add_argument("--broadcast", help="Naming: broadcast address for heartbeat", type=str)
parser.add_argument("--storage_mode", help="Storage: 0 - local mode; 1 - global mode", type=int)
parser.add_argument("--naming_ip", help="Client: naming ip to connect to", type=str)
PARAMS = parser.parse_args()

if PARAMS.module is None:
    module = int(input("""
       ▄█   ▄█▄  ▄█     ▄████████    ▄████████
      ███ ▄███▀ ███    ███    ███   ███    ███
      ███▐██▀   ███▌   ███    █▀    ███    █▀
     ▄█████▀    ███▌   ███          ███
    ▀▀█████▄    ███▌ ▀███████████ ▀███████████
      ███▐██▄   ███           ███          ███
      ███ ▀███▄ ███     ▄█    ███    ▄█    ███
      ███   ▀█▀ █▀    ▄████████▀   ▄████████▀
      ▀
    Which module do you want to start?
         0) Naming
         1) Storage
         2) Client
    """))
else:
    module = PARAMS.module

if module == 0:
    if PARAMS.internal_ip is None or PARAMS.broadcast is None:
        ipa = popen('ip a').read()
        broadcasts = set(findall("([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/[0-9]{1,2} brd ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})", ipa))
        broadcasts = sorted(list(broadcasts.union({('127.0.0.1', '255.255.255.255')})))
        print("Choose internal network with broadcast for storage nodes discovery:")
        for i in range(len(broadcasts)):
            print(f"     {i}) {broadcasts[i][0]} {broadcasts[i][1]}")
        choice = int(input(">>> "))
        internal_ip, broadcast = broadcasts[choice][0], broadcasts[choice][1]
    else:
        internal_ip, broadcast = PARAMS.internal_ip, PARAMS.broadcast
    naming = Naming(internal_ip, broadcast)
elif module == 1:
    if PARAMS.storage_mode is None:
        choice = int(input("Choose type of configuration:\n"
                           "     0) local mode (debug)\n"
                           "     1) global mode (without NAT)\n"
                           ">>> "))
    else:
        choice = PARAMS.storage_mode
    storage = Storage(choice)
elif module == 2:
    if PARAMS.naming_ip is None:
        ip = input("Enter naming ip: ")
    else:
        ip = PARAMS.naming_ip
    client = Client(ip)

# TODO docker swarm
# TODO report
# TODO presentation
